class Pessoa {
    constructor(nome) {
        this.nome = nome
    }
    falar () {
        console.log(`Meu nome é ${this.nome}`)
    }
}
const p1 = new Pessoa("Joao")
p1.falar()

// ###############################################
// Criando o mesmo exemplo da classe com uma função construtora

function CriarPessoa (nome) {
    this.nome = nome
    this.falar = () => {
        console.log(`Me chamo ${this.nome}`)
    }
}

 p = new CriarPessoa("Victor")
 p.falar()


 // Funçao arrow atribuida a uma constante
 const pessoa1 = (nome) => {
     return {
         falar: () => {
             console.log(`Me llamo ${nome}`)
         }
     }
 }
 pessoa1("Teste").falar()