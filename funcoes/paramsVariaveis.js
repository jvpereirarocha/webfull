// Semelhante ao *args do Python. arguments é um array com os parâmetros
function soma() {
    let total = 0
    for (i in arguments) {
        total += arguments[i]
    }
    return total
}
console.log(soma(1, 2, 3, 4, 5, 6, 7))
const notas = []

function verificarNotas() {
    for (i in arguments) {
        if (arguments[i] > 7) {
            console.log("Aprovado com a nota:", arguments[i])
        }
        else {
            console.log("Reprovado com a nota:", arguments[i])
        }
    }
}

verificarNotas(1.1, 2.2, 3.3, 4.4, 5.5, 6,6, 7.7, 8.8)