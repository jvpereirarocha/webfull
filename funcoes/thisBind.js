const pessoa = {
    saudacao: 'Bom dia!',
    falar() {
      console.log("Saudacao:", this.saudacao)
    }
  }
  pessoa.falar()
  const falar = pessoa.falar
  falar() //Conflito entre paradigmas: Funcional e OO
  const falarDePessoa = pessoa.falar.bind(pessoa)
  falarDePessoa()



  // ################################################################

  function Pessoa() {
    this.idade = 0
    const self = this
  
    setInterval(function(){
      self.idade++
      console.log(this.idade)
    }/*.bind(this)*/, 1000)
  }
  
  new Pessoa