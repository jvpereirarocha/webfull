const soma1 = (a, b, c) => {
    a = a || 1
    b = b || 1
    c = c || 1
    return a + b + c
}
console.log(soma1(), soma1(3), soma1(1, 2, 3), soma1(0, 0, 0))

//Estratégia para valores padroes

function soma2(a, b, c) {
    a = a != undefined ? a : 1 //Verifica se a é undefined
    b = 1 in arguments ? b : 1 // Verifica se há um parâmetro no índice 1 dos argumentos (2º parametro)
    c = isNaN(c) ? 1 : c //Verifica se o valor de não é um número
}

//Valor padrạo do es2015
const soma3 = (a = 1, b = 1, c = 1) => a + b + c
console.log(soma3(), soma3(2), soma3(2, 3), soma3(2, 3, 4))