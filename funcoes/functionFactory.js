//Factory simples - Função que retorna um objeto

function criarPessoa() {
    return {
        nome: 'Ana',
        sobrenome: 'Silva'
    }
}

console.log(criarPessoa())


function criarProduto(nome, preco, quantidade) {
    return {
        nome: nome,
        preco: preco,
        quantidade: quantidade,
        desconto: preco > 80 ? true : false
    }

}

console.log(criarProduto("Pasta de Dente", 1.99, 50))