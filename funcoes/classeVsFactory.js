class Pessoa {
    constructor(nome, idade, nacionalidade) {
        this.nome = nome
        this.idade = idade
        this.nacionalidade = nacionalidade
    }

    falar() {
        console.log(`Me chamo ${this.nome}, tenho ${this.idade} anos e sou ${this.nacionalidade}`)
    }
}
const p1 = new Pessoa("Joao", 23, "brasileiro")
p1.falar()

const pessoa = nome => {
    return {
        falar: () => console.log(`Meu nome é ${nome}`)
    }
}
const p2 = pessoa("Joao")
p2.falar()