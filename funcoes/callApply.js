function getPreco(imposto = 0, moeda = 'R$') {
    return `${moeda} ${this.preco * (1 - this.desconto) * (1 + imposto)}`
}

const produto = {
    nome: 'Notebook',
    desc: 0.15,
    preco: 4000,
    getPreco()
}

console.log(getPreco.call(produto))
console.log(getPreco.apply(produto))

const carro = {preco: 40000, desconto: 0.10}

/*
Appy e Call neste caso estão utilizando os atributos preco e desconto determinados no objeto carro
para que a função getPreco calcule o preço total do objeto passado
*/
console.log(getPreco.call(carro, 0.05, '$'))
console.log(getPreco.apply(carro, [0.10, 'R$']))


// ####################################
