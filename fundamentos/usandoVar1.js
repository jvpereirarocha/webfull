{
    {
        {
            {
                var sera = 'Será??'
            }
        }
    }
}
console.log(sera)

function teste() {
    var local = 123
}
teste()
//console.log(local) 
/* Gera um erro pois está fora do escopo.
    O escopo da variável local é só na função teste()
*/

// Variável é de escopo global (visível para todos) ou escopo de função (disponível só na função)


var numero = 1
{
    var numero = 2
    console.log("Dentro: ", numero)
}
console.log("Fora: ", numero)