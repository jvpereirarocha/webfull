const saudacao = "Olá" //Contexto léxico 1
/* Contexto Léxico Local físico onde
sua variável foi definida. Neste caso, no arquivo parNomeValor.js 
*/

function exec() {
    const saudacao = "Opa" //Contexto léxico 2
    return saudacao //Não gera conflito pois estão em contextos distintos
}
//Objetos são grupos aninhados de pares chave/valor
const cliente = {
    nome: "Joao",
    idade: 23,
    sexo: "Masculino",
    endereco: {
        logradouro: "Rua Antonio Soares de Melo",
        numero: 1177
    }
}
console.log(saudacao)
console.log(exec())
console.log(cliente.endereco.logradouro)