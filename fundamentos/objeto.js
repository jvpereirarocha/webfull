const produto1 = {}
//Forma de determinar um atributo para um objeto em Javascript
produto1.nome = "Celular Xiaomi Pocophone F1"
produto1.preco = 1522.00

//Outra forma de determinar atributos para um objeto
produto1["quantidade"] = 50
produto1["desconto"] = false

console.log(produto1)

//Outra forma de criar um objeto
const produto2 = {
    nome: 'Camisa Polo',
    preco: 49.99,
    quantidade: 100,
    desconto: true,
}
console.log(produto2)
console.log(typeof produto2)