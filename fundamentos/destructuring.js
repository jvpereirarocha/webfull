// Novo recurso do ECMAScript 2015

const pessoa = {
    nome: 'Joao',
    idade: 23,
    endereco: {
        logradouro: 'Rua Antonio Soares De Melo',
        numero: 1177,
        cidade: 'Betim',
    }
}

const { nome, idade } = pessoa //Tire de dentro do objeto pessoa os atributos nome e idade
console.log(nome, idade)

//Criando variável para os atributos extraídos
const { nome: n, idade: i} = pessoa

//Se não tiver os atributos no objeto
const { sobrenome, humor = true } = pessoa
console.log(sobrenome, humor)


//Desestruturando atributos aninhados
const { endereco: { logradouro, numero, cidade, cep = "32670486" } } = pessoa
console.log(logradouro, numero, cidade, cep)


// #########################################################################################


const [a] = [10]
console.log(a)

const [n1, n2, , n4, n5, , n7] = [10, 7, 9, 5, 3, 8, 1] // Desestruturação precisa ter o mesmo número de elementos
console.log(n1, n2, n4, n5, n7) //Saída 10, 7, 5, 3, 1

const [, [, nota]] = [[, 8, 8], [9, 6, 7]]
console.log(nota)


// #########################################################################################

//Função que utiliza destruturação como parâmetro para ter acesso direto aos atributos min e max
function rand({min = 0, max = 1000}) {
    const valor = Math.random() * (max - min) + min
    return Math.floor(valor)
}
const intervalo = { max: 80, min: 30} 
console.log(rand(intervalo))

console.log(rand({ min: 988 }))

// #########################################################################################

function aleat([min = 0, max = 1000]) {
    if (min > max) {
        [min, max] = [max, min]
    }
    const valor = Math.random() * (max - min) + min
    return Math.floor(valor)
}
console.log(aleat([50, 40]))
console.log(aleat([992]))
console.log(aleat([, 10]))
console.log(aleat([]))