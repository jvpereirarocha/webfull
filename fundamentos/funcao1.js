//Função sem retorno
function imprimirSoma(a, b) {
    console.log(a + b)
}

imprimirSoma(2, 3) //Retorna 5
imprimirSoma(2) //Retorna NaN
imprimirSoma(2,6,4,5,8,9) //Ignora os parametros excessivos, utilizando só os dois primeiros


//Função com retorno
//Determinando o b com um valor default (nesse caso 1)
function soma(a, b = 1) {
    return a + b
}

console.log(soma(2, 9)) //Retorno é 11
console.log(soma(2))
/* Retorno é 3, pois, como  o 2º parâmetro não foi especificado
* o valor default é usado no cálculo. Neste caso 2 + 1 = 3
*/