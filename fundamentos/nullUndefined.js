/* O valor de objeto aponta para referência na memória.
    Ex: Se um objeto 'b' receber o valor de um objeto 'a'
    e um atributo dentro de qualquer um desses objetos for alterado, a mudança
    será refletida em ambos os objetos pois os mesmos trabalham com referência
*/
let a = {nome: 'Joao'}
let b = a
//console.log(a, b)
a.nome = 'Maria'
console.log("Objeto A: ", a) //Saída Objeto A:  { nome: 'Maria' }
console.log("Objeto B: ", b) //Saída Objeto B:  { nome: 'Maria' }


/* Os tipos primitivos são independentes
    Ex: Suponhamos que uma variável 'y' recebe um tipo primitivo (number, string, array).
    O valor de 'y' é 3 (tipo primitivo number).
    Suponhamos que uma variável 'x' recebe o valor atribuído à variável 'y'. 
    Suponhamos que o valor de 'x' foi alterado para 5.
    Ao final dos testes, o valor de 'x' será 5. Mas o valor de 'y' continuará sendo 3
*/

let y = 3
let x = y
x = 5
console.log("Valor de y: " + y)
console.log("Valor de x: " + x)


/* #################### Null e Undefined */
let valor
console.log(valor)

valor = null //Ausência de valor. Não está apontando para nenhum endereço de memória
console.log(valor)

/* Valor null deve ser setado para uma variável que aponte para um objeto que faça uso
    de um endereço de memória
*/

const produto = {}
console.log(produto.preco) //Saída será undefined
produto.preco = 3.50
console.log(produto)

produto.preco = undefined //Evitar atribuir undefined explicitamente
console.log(!!produto.preco)
console.log(produto)

produto.preco = null //Sem preço
console.log(!!produto.preco)
console.log(produto.preco)