function getPreco(imposto = 0, moeda = 'R$') {
    return `${moeda} ${this.preco * (1 - this.desc) * (1 + imposto)}`
}

const produto = {
    nome: 'Notebook',
    preco: 4589,
    desc: 0.15,
    getPreco,
}

console.log(getPreco())
console.log(produto.getPreco())

// Outras formas de executar com call e apply

const carro = { preco: 49990, desc: 0.20 }

console.log(getPreco.call(carro)) //call() usa o objeto carro como contexto da função getPreco
console.log(getPreco.apply(carro)) //call() usa o objeto carro como contexto da função getPreco

console.log(getPreco.call(carro, 0.17, '$')) //Método call recebe os parametros diretamente
console.log(getPreco.apply(carro, [0.17, '$'])) //Método apply recebe os parâmetros dentro de um array
