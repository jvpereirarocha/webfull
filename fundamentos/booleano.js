let isAtivo = false

isAtivo = true

isAtivo = 1
console.log(!!isAtivo) //Forma para verificar se o valor númerico é verdadeiro ou falso

//Situações quando variáveis serão verdadeiras
console.log('os verdadeiros...')
console.log(!!3)
console.log(!!-1)
console.log(!!' ')
console.log(!![])
console.log(!!{})
console.log(!!Infinity)
console.log(!!(isAtivo = true))


//Situações quando variáveis serão falsas
console.log('os falsos...')
console.log(!!0)
console.log(!!'')
console.log(!!null)
console.log(!!NaN)
console.log(!!undefined)
console.log(!!(isAtivo = false))


let nome = ''
console.log(nome || 'Desconhecido')