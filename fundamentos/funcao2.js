// Armazenando uma função dentro de uma variável ou constante

const imprimirSoma = function (a, b) {
    console.log(a + b)
}

imprimirSoma(2, 3)


// Armazenando uma função arrow em uma variável ou constante
const soma = (a,b) => {
    console.log(a + b)
}
soma(3, 6)

//Retorno implícito na arrow function
const subtracao = (a, b) => a - b //Retorna a subtração entre a e b
console.log(subtracao(3, 4))
const somarMaisCinco = a => a + 5 // Retorna o valor de a + 5
console.log(somarMaisCinco(3)) //Retorna 8