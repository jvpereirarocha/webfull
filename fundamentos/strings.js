const escola = "Cod3r"

// charAt retorna a string de acordo com a posição especificada
console.log(escola.charAt(4))
console.log(escola.charAt(5)) //Se tiver fora do índice da string, retorna um valor vazio

// Retorna o valor de unicode do caractere especificado pelo índice
console.log(escola.charCodeAt(3))
//O valor do índice 3 é o próprio número 3 que corresponde ao unicode 51

//Retorna o índice de acordo com o valor especificado da string
console.log(escola.indexOf("C")) //Neste caso, o valor 'C' está na posição 0. Será retornada a posição 0
console.log(escola.indexOf("g")) //Neste caso, não existe a letra 'g'. O valor retornado será -1

//Imprime a string a partir do índice 1. Saída será 0d3r
console.log(escola.substring(1))
//Também é possível especificar o intervalo de início e de final (não inclui o índice final)
console.log("String:", escola.substring(0, 3))
/* 
* Se especificar um índice final que esteja fora do tamanho da string,
* a função retornará toda a string
*/

//Concatena várias strings. Saída = Escola Cod3r!
console.log("Escola ".concat(escola).concat("!"))

/*Substitui valores dentro de uma string
    string.replace('old', 'new')
    OBS: Expressões regulares são suportadas
*/
console.log("Subs: ", escola.replace("3", "e"))


//Separa um string e gera um array
console.log("Ana,Maria,Pedro".split(","))