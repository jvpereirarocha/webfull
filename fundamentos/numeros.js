const peso1 = 1.1
const peso2 = Number('2.0')

console.log(peso1, peso2)
console.log(Number.isInteger(peso1))


const avaliacao1 = 9.871
const avaliacao2 = 6.871

const total = avaliacao1 * peso1 + avaliacao2 * peso2
const media = total / (peso1 + peso2)

// To fixed é utilizado para determinar o número de casas decimais a ser mostrado
console.log(media.toFixed(2))

//Converte um número para string
console.log(media.toString())

//Converte para binário
console.log(media.toString(2))

// typeof mostra o tipo da variável
console.log(typeof media)


/* ALGUNS CUIDADOS AO SE TRABALHAR COM NÚMEROS */

console.log(7 / 0.0001)
console.log(7 / 0)
console.log("10" / 2)
console.log("3" + 2) //Concatena. Não soma
console.log("Show!" * 2)
console.log(0.1 + 0.7)
