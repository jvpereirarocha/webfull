const a = 7 //Atribuindo o valor 7 à constante 'a'
let b = 3 //Atribuindo o valor 3 à variável b

b += a // Equivalente: b = b + a
b -= a //Equivaliente: b = b - a
b *= 2 //Equivalente: b = b * 2
b /= 2 //Equivalente: b = b / 2
b %= 2 //Equivalente: b = b % 2 (Resto de divisão) 