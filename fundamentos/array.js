const valores = [7.7, 8.8, 9.2, 9.4]
console.log(valores[0], valores[3])
console.log(valores[4])

valores[4] = 10
valores[13] = 33
console.log(valores)

//Determina a quantidade de elementos do array
console.log(valores.length)

//Outra forma de inserção em arrays
valores.push("id")
console.log(valores)

//Retira o último valor do array
valores.pop()
console.log(valores)

//Outra forma de excluir valores do array
delete valores[0] //Retira o valor de índice 0 (primeiro valor) do array
console.log(valores)

console.log(typeof valores)