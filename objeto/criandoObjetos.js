//Notação literal
const obj1 = {}
console.log(obj1)

//Object em JS
console.log(typeof Object, typeof new Object)
const obj2 = new Object
console.log(obj2)

console.log("Teste")

//Funções construtoras
function Produto(nome, preco, desc) {
    this.nome = nome
    this.getPrecoComDesconto = () => {
        return preco * (1- desc)
    }
}

const p1 = new Produto('Caderno', 6.99, 0.2)
const p2 = new Produto('Caneta', 1.49, 0.01)

console.log(`O valor do produto ${p1.nome} é R$ ${p1.getPrecoComDesconto().toFixed(2)}`)

//Função Factory
function criarFuncionario(nome, salarioBase, numFaltas) {
    return {
        nome,
        salarioBase,
        numFaltas,
        getSalario() {
            return (salarioBase / 30) * (30 - numFaltas)
        }
    }
}

const func1 = criarFuncionario('Joao', 5000, 4)
const func2 = criarFuncionario('Maria', 6500, 2)

console.log(`O funcionário ${func1.nome} teve ${func1.numFaltas} faltas e seu salário no mês foi
    ${func1.getSalario().toFixed(2)}`)

    console.log(`A funcionária ${func2.nome} teve ${func2.numFaltas} faltas e seu salário no mês foi
        ${func2.getSalario().toFixed(2)}`)

//Object.create
const filha = Object.create(null)
filha.nome = 'Ana'
console.log(filha)

//Função famosa que retorna um objeto
const fromJSON = JSON.parse('{"info": "My info"}')
console.log(fromJSON.info)
