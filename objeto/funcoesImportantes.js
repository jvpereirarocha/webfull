const pessoa = {
    nome: 'Joao Victor',
    idade: 23,
    peso: 64.5,
}

console.log(Object.keys(pessoa))
console.log(Object.values(pessoa))
console.log(Object.entries(pessoa))


Object.entries(pessoa).forEach(([chave, valor]) => {
    console.log(`${chave}: ${valor}`)
})

Object.defineProperty(pessoa, 'dataNascimento', {
    enumerable: true, //Será enumerado na lista
    writable: false, //Não permite ser sobrescrito
    value: '27/02/1997'
})

pessoa.dataNascimento = '01/02/2017' //Foi definido que não é possível alterar a propriedade

console.log("Data de Nascimento: ", pessoa.dataNascimento)


//Função no ES2015

const dest = { a: 1 }
const o1 = { b: 2 }
const o2 = { c: 3, a: 4}
const obj = Object.assign(dest, o1, o2) //Coloca dentro do 1º objeto os valores dos outros objetos
//OBS: O valor de a existe no objeto de destino mas será sobrescrito pelo valor de a do objeto 'o2'
console.log(obj)
