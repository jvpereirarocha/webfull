const pai = {
	nome: 'Pedro',
	corCabelo: 'preto'
}

const filha1 = Object.create(pai)
filha1.nome = 'Ana'
console.log(filha1.corCabelo)

const filha2 = Object.create(pai, {
	nome: {value: 'Bia', writable: false, enumerable: true},
	
})

console.log(filha2.nome)

filha2.nome = 'Carla' //Objeto não pode ser editado, portanto, o nome Carla não será setado
console.log(`${filha2.nome} tem cabelo ${filha2.corCabelo}`)

console.log(Object.keys(filha1))
console.log(Object.keys(filha2))


//hasOwnProperty verifica se o atributo pertence ao próprio objeto ou foi herdado
for(let key in filha2) {
	if(filha2.hasOwnProperty(key)) {
		console.log(key)
	}
	else {
		console.log('Por herança:', key)
	}
}


for (let chave in pai) {
	console.log(chave)
}





