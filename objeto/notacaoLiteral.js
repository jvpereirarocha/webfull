const a = 1
const b = 2
const c = 3

//Antiga forma de criar objeto (com duplicidade)
const obj1 = { a: a, b: b, c: c}

//Nova forma no ES2015
const obj2 = {a, b, c} //O intepretador entende que está sendo o valor de a ao atributo de mesmo nome


const nomeAttr = 'nota'
const valorAttr = 7.87

const obj3 = {}
obj3[nomeAttr] = valorAttr
