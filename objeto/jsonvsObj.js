const obj = {a: 1, b: 2, c: 3, soma() { return a + b + c}}
console.log(obj)

console.log(JSON.stringify(obj)) // Convertendo um objeto javascript para o formato JSON. A função soma não é convertida

// console.log(JSON.parse("{a: 1, b: 2, c: 3, }"))

const json = '{ "a": 1, "b": 2, "c": 3, "d": 4}'
console.log(JSON.parse(json))
console.log(JSON.parse('{"a": 1, "b": "string", "c": true, "d": {}, "e": [] }'))
