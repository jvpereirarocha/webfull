//Object.preventExtensions -> permite excluir e alterar os valores dos atributos mas nao permite adicionar novos

const produto = Object.preventExtensions({
	nome: 'Qualquer',
	preco: 1.99,
	tag: 'promoção'
})

console.log('Extensível:', Object.isExtensible(produto))

produto.nome = 'Borracha'
produto.descricao = 'Borracha branca escolar' // O atributo descricao nao será criado pois o objeto não é extensível
delete produto.tag
console.log(produto)

//Object.seal -> Permite alterar os atributos mas não é possível adicionar nem excluir atributos
const pessoa = { nome: 'Juliana', idade: 35 }

Object.seal(pessoa)
console.log('Selado:', Object.isSealed(pessoa))

// Object.freeze -> Congela totalmente o atributo. Não é possível adicionar, nem excluir nem alterar atributos
