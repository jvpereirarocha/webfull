console.log(typeof String)
console.log(typeof Array)
console.log(typeof Object)

//É possível adicionar funcionalidades a um tipo. Como por exemplo, adicionar a função reverse a strings
String.prototype.reverse = function () {
	return this.split('').reverse().join('')
}

console.log('Teste'.reverse())

Array.prototype.first = function () {
	return this[0]
}

const a = [1, 3, 5, 7]
console.log(a.first())
