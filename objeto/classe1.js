class Lancamento {

	constructor(nome = 'Generico', valor = 0) {
		this.nome = nome
		this.valor = valor
	}
	
}

class CicloFinanceiro {
	constructor(mes, ano) {
		this.mes = mes
		this.ano = ano
		this.lancamentos = []
	}
	
	addLancamentos(...lancamentos) {
	
		lancamentos.forEach(l => this.lancamentos.push(l))
	}
	
	sumario() {
		let valorTotal = 0
		this.lancamentos.forEach((l) => {
			valorTotal += l.valor
		})
		return valorTotal
	}
}

const salario = new Lancamento('Salario', 4500)
const contaDeLuz = new Lancamento('Conta de Luz', -200)

const ciclo = new CicloFinanceiro(6, 2018)

ciclo.addLancamentos(salario, contaDeLuz)

console.log(ciclo.sumario())

















