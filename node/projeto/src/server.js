const port = 3003

const express = require('express')
const app = express()
const db = require('./database')
const bodyParser = require('body-parser')

// Função 'use' abaixo é chamada em qualquer endpoint que for feito a requisição
// app.use((request, response, next) => {
//     response.send("Default endpoint")
// })

app.use(bodyParser.urlencoded({ extended: true }))
//Utilizando o body parser para converter o corpo da requisição

//Rota inicial
app.get('/', (request, response, next) => {
    response.send('Welcome to the app')
})

//Pegando todos os produtos
app.get('/produtos', (request, response, next) => {
    //response.send({name: 'Notebook', pricing: 4000.00 }) //Converter para JSON automaticamente
    response.send(db.getAllProducts())
})

//Criando endpoint que tem parâmetro para pegar informações de um determinado produto
app.get('/produtos/:id', (request, response, next) => {
    response.send(db.getProduct(request.params.id))
})

//Criando produto
app.post('/produtos', (request, response, next) => {
    const product = db.saveProduct({
        name: request.body.name,
        pricing: request.body.pricing,
        descount: request.body.descount,
    })
    response.send(product) //Converte parta JSON
})

//Alterando produto
app.put('/produtos/:id', (request, response, next) => {
    const product = db.saveProduct({
        id: request.params.id,
        name: request.body.name,
        pricing: request.body.pricing,
        descount: request.body.descount,
    })
    response.send(product) //Converte parta JSON
})

app.delete('/produtos/:id', (request, response, next) => {
    const product = db.deleteProduct(request.params.id)
    response.send(product)
})

//Determinando porta
app.listen(port, () => {
    console.log(`The server is running at the port ${port}.`)
})