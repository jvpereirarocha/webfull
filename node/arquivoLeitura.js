const fs = require('fs')

const caminho = __dirname + '/arquivo.json'

//Lendo o arquivo de forma síncrona
const conteudo = fs.readFileSync(caminho, 'utf-8')
console.log(conteudo)


//Lendo de forma assíncrona
fs.readFile(caminho, 'utf-8', (error, content) => {
    const config = JSON.parse(content)
    console.log(`${config.db.host}: ${config.db.port}`) 
})

//Lendo um json
const config = require('./arquivo.json')
console.log(config.db)

//Lendo um diretório
fs.readdir(__dirname, (err, arquivos) => {
    console.log(arquivos)
})