const fs = require('fs')

const produto = {
    nome: 'Celular',
    preco: 1549.99,
    quantidade: 250,
    desconto: 0.15
}

//Escrevendo em um arquivo
fs.writeFile(__dirname + '/arquivoGerado.json', JSON.stringify(produto), error => {
    console.log(error || "Arquivo salvo com sucesso!")
})