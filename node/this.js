console.log(this === global) //false
console.log(this === module) //false

console.log(this === module.exports) //true
console.log(this === exports) //true

function logThis() {
    console.log('Dentro de uma função...')
    console.log(this === exports) //Dentro de uma função é false
    console.log(this === module.exports) //Dentro de uma função é false
    console.log(this === global) //Dentro da função, é true

    this.perigo = '...' //Está incluindo 'perigo' dentro do escopo global
}

logThis()
