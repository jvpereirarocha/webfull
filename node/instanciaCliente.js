const contadorA = require('./instanciaUnica')
const contadorB = require('./instanciaUnica')

const contadorC = require('./instanciaNova')() //Invocando uma função
const contadorD = require('./instanciaNova')()

contadorA.inc()
contadorA.inc()

console.log(contadorA.valor, contadorB.valor) //Com cache

contadorC.inc()
contadorC.inc()

console.log(contadorC.valor, contadorD.valor) //Sem cache