//Map é uma estrutura chave valor, semelhante ao objeto

const tecnologias = new Map()
tecnologias.set('react', { framework: false })
tecnologias.set('angular', { framework: true })

//Acessando map
console.log(tecnologias.get('react'))

const chavesVariadas = new Map([
    [function () {}, 'Função'],
    [{}, 'Objeto'],
    [123, 'Numero'],
])

chavesVariadas.forEach((vl, ch) => {
    console.log(ch, vl)
})

console.log(chavesVariadas.has(123))
chavesVariadas.delete(123) //Deleta um valor
console.log(chavesVariadas.has(123)) //Verifica se o valor existe no map
console.log(chavesVariadas.size) //Pega o tamanho do map
