{
    var a = 2
    let b = 3
}

console.log(a) //var não possui escopo de bloco. Será mostrado normalmente
// console.log(b) /* Neste escopo, a variável let não é listada. Existe escopo de bloco para let /*

//Template String
const produto = 'iPad'
console.log(`${produto} é caro`)

//Quebrando string em mais linhas
console.log(`${produto}
    é caro`)


//Destructuring
const [l, e, ...tras] = 'Cod3r' //Funciona também para string
console.log(l)
console.log(e)
console.log(tras)

const [x, y, z ]= [1, 2, 3] //Destructuring array

const { idade, nome } = { nome: 'Ana', idade: 9 }
console.log(idade, nome)

const { idade: i, nome: n } = { nome: 'Joao', idade: 23 } //Para alterar o nome da propriedade do objeto
console.log(i, n)
