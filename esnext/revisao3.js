// ES8: Object.values / Object.entries

//Object.values -> Pega os valores do objeto
//Object.entries -> Pega chave e valor do objeto

const obj = {
    a: 1,
    b: 2,
    c: 3,
    d: 4
}

console.log(Object.values(obj)) //Imprime os números 1, 2, 3 e 4 em forma de array
console.log(Object.entries(obj)) //Imprime uma matriz com chave e valor do objeto


//Melhorias na Notação Literal

const nome = 'Carla'
const pessoa = {
    nome, //Cria um objeto com o valor nome:nome
    ola() {
        return 'Oi gente'
    }
}
console.log(pessoa.ola())


//Class

class Animal {}
class Cachorro extends Animal {
    falar() {
        return 'Au Au!'
    }
}

console.log(new Cachorro().falar())
