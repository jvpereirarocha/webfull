// Estrutura de conjunto não indexada e não aceita repetições

const times = new Set()
/* Adicionando items com .add(item) */
times.add('Inter')
times.add('Gremio')
times.add('Corinthians')
times.add('Vasco')
times.add('Inter') //Não será adicionado pois já existe no set e set não aceita repetição

console.log(times)

console.log(times.size) //Verifica tamanho do set
console.log(times.has('Inter')) //Verifica se o item existe dentro do set
times.delete('Gremio') //Excluindo items
