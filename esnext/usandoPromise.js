// Com promise

const http = require('http')

const getTurmas = (letra) => {
    const url = `http://files.cod3r.com.br/curso-js/turma${letra}.json`
    return new Promise ((resolve, reject) => {
        http.get(url, response => {
            let resultado = ''

            response.on('data', dados => {
                resultado += dados
            })

            response.on('end', () => {
                try {
                    resolve(JSON.parse(resultado))
                } catch(e) {
                    reject(e)
                }
            })
        })
    })
}

let nomes = []

//Promise.all([]) recebe um array com as promises

Promise.all([getTurmas('A'), getTurmas('B'), getTurmas('C')])
    .then(data => [].concat(...data))
    .then(alunos => alunos.map(aluno => aluno.nome))
    .then(response => console.log(response))
    .catch(e => console.log("Error: ", e.message))


    // .then(alunos => alunos.map(aluno => aluno.nome))
    // .then(response => console.log(response))
