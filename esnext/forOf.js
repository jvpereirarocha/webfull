for (let letra of 'Cod3r') {
    console.log(letra)
}

const assuntosEcma = ['Map', 'Set', 'Promise']

// For in percorre array e a variável i retorna os índices
for (let i in assuntosEcma) {
    console.log(i)
}

// For of percorre o array e a variável l retorna os valores
for (let l of assuntosEcma) {
    console.log(l)
}

const assuntosMap = new Map([
    ['Map', { abordado: true }],
    ['Set', { abordado: true }],
    ['Promise', { abordado: false }],
])

for (let assunto of assuntosMap) {
    console.log(assunto)
}

for (let chave of assuntosMap.keys()) {
    console.log(chave)
}

for (let valor of assuntosMap.values()) {
    console.log(valor)
}

for (let [ch, vl] of assuntosMap.entries()) {
    console.log(`Chave ${ch} - Valor: ${vl.abordado}`)
}

const s = new Set([
    'a', 'b', 'c'
])
for (let letra of s) {
    console.log(letra)
}
