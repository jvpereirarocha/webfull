// Operador ... Rest (juntar) / Spread (espalhar)
// Usar rest como parâmetro de função

// usar spread com objeto
const funcionario = {
    nome: 'Maria',
    salario: 5000.00
}
const clone = { ativo: true, ...funcionario } //Pegando os atributos de funcionário e espalhando para a constante clone
console.log(funcionario)
console.log(clone)

// Usar spread com array
const grupoA = ['Joao', 'Pedro', 'Maria']
const grupoFinal = ['Flavia', 'Wellington', ...grupoA]
console.log(grupoFinal)
