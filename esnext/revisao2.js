/* Arrow function */

const soma = (a, b) => a + b //Retorno implícito
console.log(soma(2, 4))

//Se tiver só um parâmetro, é possível ocultar o parêntese
const sayMyName = name => name.toUpperCase()

console.log(sayMyName('Joao'))



// this

//Não é possível mudar o contexto do this em arrow functions
const lexico1 = () => console.log(this === exports)
const lexico2 = lexico1.bind({})
lexico1()
lexico2()


//Parametro default
function log(texto = 'Node') {
    console.log(texto)
}

log('Teste')



// Operador rest ou spread
function total(...numeros) {
    let total = 0
    numeros.forEach(n => total += n)
    return total
}
console.log(total(2, 3, 4, 5))
