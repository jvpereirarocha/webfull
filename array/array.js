console.log(typeof Array, typeof new Array, typeof [])

let aprovados = new Array('Carlos', 'Bia', 'Ana') //Forma não recomendada de criar um array
console.log(aprovados)

aprovados = ['Bia', 'Carlos', 'Ana'] //Notação literal. Forma recomendada
console.log(aprovados[0])

aprovados[3] = 'Paulo' //Forma de adicionar em um array
aprovados.push('Abia')
console.log(aprovados.length)


//Se adicionar no índice 9

aprovados[9] = 'Rafael'
console.log(aprovados.length)
// Ao adicionar no índice 9 os índices (que anteriormente não existiam) passam a ser undefined [5], [6], [7], [8]
console.log(aprovados[8] === undefined) // Saída: true
console.log(aprovados[8] === null) // Saída: false

console.log(aprovados)

//Funções do array
aprovados.sort() //Ordena os elementos, neste caso, por ordem alfabética
console.log(aprovados)

delete aprovados[1] //Excluindo o valor do índice 1. O índice 1 fica undefined
console.log(aprovados)

aprovados = ['Bia', 'Carlos', 'Ana']
aprovados.splice(1, 1) //.splice(index, n° elementos a serem excluídos)
console.log(aprovados)
aprovados.splice(1, 0, 'Joao', 'Felipe') //Neste caso, dois elementos são adicionados ao array. Nenhum será excluído
console.log(aprovados)
