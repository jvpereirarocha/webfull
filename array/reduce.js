//reduce() como o nome já diz, reduz um conjunto de dados em um único resultado

// O exemplo abaixo soma os valores de notas de alunos e calcula a média aritmética

//Parâmetros -> reduce(acumulador, valorAtual)

const notas = [7.5, 8.9, 5.7, 6.4, 9.2, 4.8, 10]

const getTotalNotas = (total, nota) => total + nota

const calculaMedia = parseFloat(notas.reduce(getTotalNotas) / notas.length).toFixed(2)

console.log(`A média das notas é ${calculaMedia}`)
