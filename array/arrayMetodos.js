const pilotos = ['Vettel', 'Alonso', 'Raikkonen', 'Massa']
//pop()
pilotos.pop() // Remove o último elemento do array
console.log(pilotos)

pilotos.push('Verstappen') //Adiciona um elemento na última posição do array
console.log(pilotos)

pilotos.shift() //Remove o primeiro elemento do array
console.log(pilotos)

pilotos.unshift('Hamilton') //Adiciona um elemento na primeira posição do array
console.log(pilotos)


// Splice pode adicionar ou remover elementos do array

//Adicionando
pilotos.splice(2, 0, 'Bottas', 'Massa') //Adiciona dois elementos a partir da posição 2 do array. Nao remove nenhum
console.log(pilotos)

//Removendo
pilotos.splice(3, 1) //Remove um elemento na posição 3
console.log(pilotos)

//Slice
const algunsPilotos1 = pilotos.slice(2) //Gera novo array a partir do índice especificado (neste caso 2)
console.log(algunsPilotos1)

const algunsPilotos2 = pilotos.slice(1, 4) //Gera um novo array do índice 1 (incluindo) até o índice 4 (excluindo)
console.log(algunsPilotos2)
