Array.prototype.map2 = function(callback) {
    const newArray = []
    for (let i = 0; i < this.length; i++) {
        newArray.push(callback(this[i], i, this))
    }

    return newArray
}


const carrinho = [
    '{"nome": "Borracha", "preco": 3.45}',
    '{"nome": "Caderno", "preco": 13.90}',
    '{"nome": "Kit de Lápis", "preco": 41.22}',
    '{"nome": "Caneta", "preco": 3.50}',
]

//Retornar um array apenas com os preços

const toObject = e => JSON.parse(e)
const getPreco = e => e['preco']
const formatarPreco = e => e.toFixed(2).replace('.', ',')

let resultado = carrinho.map2(toObject).map2(getPreco).map2(formatarPreco)

console.log(resultado)
