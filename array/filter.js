//filter() como o nome já diz, filtra um array de acordo com um critério estabelecido através de uma função callback
const idades = [18, 15, 17, 20, 25, 32, 14, 11, 10]

const getMaiores = idade => idade >= 18

const maiores = idades.filter(getMaiores)
console.log(maiores)

const menores = idades.filter(function(idade){
    return idade < 18
})

console.log(menores)


Array.prototype.myFilter = function(callback) {
    const newArray = []
    for (let i = 0; i < this.length; i++) {
        if (callback(this[i], i, this)) {
            newArray.push(this[i])
        }
    }

    return newArray
}


const notas = [7, 8, 5, 6, 10, 4]

const getAprovados = nota => nota >= 6

const aprovados = notas.myFilter(getAprovados)

console.log(aprovados)
