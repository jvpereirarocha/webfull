const carrinho = [
    '{"nome": "Borracha", "preco": 3.45}',
    '{"nome": "Caderno", "preco": 13.90}',
    '{"nome": "Kit de Lápis", "preco": 41.22}',
    '{"nome": "Caneta", "preco": 3.50}',
]

//Retornar um array apenas com os preços

const toObject = e => JSON.parse(e)
const getPreco = e => e['preco']
const formatarPreco = e => e.toFixed(2).replace('.', ',')

let resultado = carrinho.map(toObject).map(getPreco).map(formatarPreco)

console.log(resultado)
