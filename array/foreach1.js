const aprovados = ['Agatha', 'Aldo', 'Daniel', 'Raquel']

//forEach(elemento, indice, array)
aprovados.forEach(function(nome, indice){
    console.log(`${indice + 1}) ${nome}`)
})

aprovados.forEach(nome => console.log(nome)) //Também suporta arrowFunction

const exibirAprovados = aprovado => console.log(aprovado)

aprovados.forEach(exibirAprovados)

aprovados.forEach(function(elemento, indice, array) {
    let fullName = `${elemento} Rocha`
    array.push(fullName)
})
console.log(aprovados)
