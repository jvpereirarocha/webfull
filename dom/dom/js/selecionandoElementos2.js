const selecionar = (seletor) => {
    document.querySelectorAll(seletor)
    .forEach(e => e.classList.add('destaque'))
}

const deselecionar = (seletor) => {
    document.querySelectorAll(seletor)
    .forEach(e => e.classList.remove('destaque'))
}

selecionar('div')
deselecionar('.terceiro.ultimo')
deselecionar('*')
selecionar(':not(#primeiro)')
selecionar(':nth-child(1)')
deselecionar(':not(.terceiro)')
deselecionar('*')
selecionar('[wb-attrib]')
