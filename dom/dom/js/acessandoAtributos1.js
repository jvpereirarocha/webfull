const img = document.querySelector('img')
console.log('getAttribute', img.getAttribute('src'))
console.log('src', img.src)
console.log('["src"]', img['src'])
console.log('alt', img.alt)

console.log('Elemento img.......')
console.log('Node Type', img.nodeType)
console.log('Node Name', img.nodeName)

const src = img.getAttributeNode('src')
console.log('Atributo src........')
console.log('Node Type', src.nodeType)
console.log('Node Name', src.nodeName)
console.log('Node Value', src.nodeValue)

const link = document.querySelector('a')
console.log('Elemento a............')
console.log('href', link.href)
console.log('firstChild.nodeType', link.firstChild.nodeType)
link.firstChild.nodeValue = 'Novo texto'
link.innerHTML = 'Novo Texto 2'
