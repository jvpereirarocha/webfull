function log () {
    console.log(this)
}
HTMLCollection.prototype.log = log
HTMLElement.prototype.log = log
NodeList.prototype.log = log
Node.prototype.log = log

const lista = document.querySelector('div ul')
lista.parentNode.log() //Elemento pai da lista
lista.childNodes.log() //Elemento filho da lista

const primeiro = lista.firstChild
primeiro.nextSibling.log()

const ultimo = lista.lastChild
ultimo.previousSibling.log()

lista.children.log()
lista.firstElementChild
lista.lastElementChild
lista.parentNode.previousElementSibling
lista.parentNode.nextElementSibling

document.querySelectorAll('div ul li')
