const attrName = 'escritorio'
const lista = document.querySelector(`div ul[destino=${attrName}]`)
console.log('Elemento ul.....................')
console.log('childNodes', lista.childNodes)
console.log('destino', lista.getAttribute('destino'))
console.log('hasAttribute', lista.hasAttribute('destino')) //Retorna true ou false
lista.setAttribute('destino', 'empresa') //.setAttribute(atributo, valor)
console.log('destino', lista.getAttribute('destino'))
lista.setAttribute('status', 'aberto') //Cria um novo atributo se não existir
lista.removeAttribute('destino') //Remove um atributo

console.log('lista.dataset', lista.dataset)

lista.setAttribute('data-teste', 'ola')
console.log('lista.dataset', lista.dataset)

const dataAttrs = Array.from(lista.dataset)

console.log('Array..................')
dataAttrs.forEach((e) => {
    console.log(e)
})
