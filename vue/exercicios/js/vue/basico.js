//Importantes coisas na Vue Instance

/*
    el: elemento HTML a ser controlado
    data: objeto para lidar com os dados necessários
    methods: Métodos utilizados na instância em questão
    computed: Funções que calculam algo em cima dos dados
*/

new Vue({
    el: "#app",
    data: {
        teste: 'aaaaa',
        nome: 'João Victor',
        idade: 23,
        profissao: 'Programador'
    },
    methods: {
        nowDate() {
            return new Date().toLocaleString()
        }
    },
    computed: {
        birthYear() {
            return 2020 - this.idade
        }
    }
})
