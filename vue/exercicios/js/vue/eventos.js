new Vue({
    el: '#evento',
    data: {
        contador: 0,
        x: 0,
        y: 0,
    },
    methods: {
        add() {
            this.contador++
        },
        sub() {
            if (this.contador > 0) {
                    this.contador = this.contador - 1
            }
            else {
                alert('Não foi possível subtrair. o contador já está zerado')
            }
        },
        atualizarPosicao(event) {
            this.x = event.clientX
            this.y = event.clientY
        },
        stop(event) {
            event.stopPropagation()
        },
        alerta() {
            alert('Enter')
        }
    }
})

/*
    diretiva v-on:evento
    Exemplo: v-on:click="metodo"
    Cada vez que for clicado, o método especificado é chamado

    Outra forma de chamar
    @evento="metodo"

    OBS: É possível colocar uma expressão simples

    evento.stop -> Para o comportamento padrao do evento

    Outra forma seria criar uma função com o método
    evento.stopPropagation()
*/
