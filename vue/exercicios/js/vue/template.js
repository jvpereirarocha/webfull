new Vue({
    el: '#app',
    data: {
        titulo: 'VueJs',
        subtitulo: 'Framework Progressivo',
    },
    template: `
        <div class="teste">
            <p> Olá, este é um teste </p>
            <p> Outro Teste </p>
        </div>
    `,
    methods: {
        ola() {
            alert('Olá!')
        }
    }
})
