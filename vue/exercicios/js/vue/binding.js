new Vue({
    el: '#binding',
    data: {
        nome: "Joao",
        link: 'https://youtube.com/cod3rcursos'
    },
    methods: {

    },
    watch: {
        nome(novoNome) {
            console.log(novoNome.toUpperCase())
        }
    }
})

//v-bind é uma diretiva utilizada para links, por exemplo
// sintaxe: v-bind:href="nomeDaPropriedade"

//Outra sintaxe possível é omitindo v-bind e só colocando ':' como abaixo
// :href="nomedaPropriedade"


//v-model é um bind bidirecional, ou seja, ao inserir o v-model e
// atualizar o valor da propriedade passada, o comportamento será
// usado imediatamente pela diretiva

/*
    Exemplo de uso:
    <input type="text" v-model="nome"/>
    <span> {{ nome }} </span>

    No exemplo acima, a diretiva irá procurar uma propriedade chamada
    'nome' na instancia do Vue. Assim que o valor do input for mudado
    isso será refletido na tag span que está abaixo, dando uma impressão
    de mudança em tempo real
*/


/*
    v-bind:value não é uma diretiva bidirecional, ou seja, se atualizar
    o valor da input, isso não será refletido na tag span abaixo
*/


/*
    watch -> Utilizada para responder a alteração de um atributo
    Trabalha diretamente com a diretiva v-model
*/
