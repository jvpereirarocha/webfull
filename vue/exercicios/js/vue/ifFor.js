new Vue ({
    el: '#app',
    data: {
        alunos: [
            { nome: "Ana", nota: 7.8 },
            { nome: "Bia", nota: 9.1 },
            { nome: "Carlos", nota: 6.8 },
            { nome: "Daniel", nota: 2.8 }
        ]
    },
})

/*
    Diretivas v-for e v-if

    Exemplo de for:
    <li v-for="aluno in alunos">
        {{ aluno.nome }}
    </li>
    No exemplo acima, a diretiva v-for percorre
    um array de alunos e lista o nome de cada um deles

    Exemplo de if:
    <span v-if="aluno.nota >= 7">
        Aprovado
    </span>

    <span v-else-if="aluno.nota >= 5 && aluno.nota < 7">
        Recuperação
    </span>

    <span v-else>
        Reprovado
    </span>

    v-show
    Mostra os elementos de acordo com um critério mas
    mantém os outros elementos com display none

    Ex v-show:

    <ul>
        <li v-for="n in 20">
            <span v-show="n % 2 == 0">
                {{ n }}
            </span>
        </li>
    </ul>

    Mostra somente os números pares e os ímpares ficam
    como display none
*/
