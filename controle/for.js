for (let x = 0; x < 10; x++) {
    console.log(`x = ${x}`)
}

const notas = [2, 3, 5, 7, 9, 10, 5, 6, 3, 1, 4]

for (let i = 0; i < notas.length; i++) {

    if (notas[i] >= 7) {
        console.log("Aprovado com a nota ", notas[i])
    }
    else {
        console.log("Reprovado com a nota ", notas[i])
    }
}

for (i in notas) {
    console.log(`Índice ${i}, Nota ${notas[i]}`)
}

dados = {
    nome: "jv",
    idade: 23,
    nacionalidade: "brasileiro",
    estado: "mg",
    endereco: {
        rua: "antonio soares de melo",
        numero: 1177,
        bairro: "betim industrial"
    },
    profissao: "programador"
}

for (atributo in dados) {
    if ('endereco' == dados[atributo]) {
        console.log("Endereco")
    }
}