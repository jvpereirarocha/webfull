nota = 10

switch(nota) {
    case 10:
        console.log("Maior nota")
        break
    case 9:
        console.log("Boa nota")
        break
    case 5:
        console.log("Recuperação")
        break
    default:
        console.log("Nota inválida")
}