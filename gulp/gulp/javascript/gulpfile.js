const gulp = require('gulp')
const { series } = require('gulp')
const concat = require('gulp-concat')
const uglify = require('gulp-uglify')
const babel = require('gulp-babel')


function main(callback) {
    gulp.src('src/**/*.js')
        .pipe(babel({
            comments: false, //Exclui os comentários e só converte o código em si
            presets: ['env'] //Pega a versão mais recente do ecmascript
        }))
        .pipe(uglify()) //Código em uma linha só, sem espaços
        .on('error', function(err){
            console.log(err) //É possível passar um evento no meio da transformação do código
        })
        .pipe(concat('codigo.min.js')) //Concatena todos os arquivos dentro de um só
        .pipe(gulp.dest('build')) //Insere o arquivo final dentro de uma pasta build, que será criada

    return callback()
}

//Outra forma de chamar o gulp sem precisar de retornar uma função callback
function teste() {
    return gulp.src('src/**/*.js')
    .pipe(gulp.dest('copias'))
}

exports.default = series(main, teste)
