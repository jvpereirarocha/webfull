const gulp = require('gulp')
const webServer = require('gulp-webserver')
const watch = require('gulp-watch')


function servidor() {

    return gulp.src('build')
        .pipe(webServer({
            port: 6060,
            open: true,
            livereload: true,
        }))
}

function monitorarArquivos(callback) {
    watch('src/**/*.html', () => gulp.series('appHTML')())
    watch('src/assets/sass/**/*.scss', () => gulp.series('appCSS')())
    watch('src/assets/js/**/*.js', () => gulp.series('appJS')())
    watch('src/assets/imgs/**/*.*', () => gulp.series('appIMG')())

    return callback()
}


module.exports = {
    servidor,
    monitorarArquivos
}
