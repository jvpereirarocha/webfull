const gulp = require('gulp')

//Gulp faz a tarefa de forma sequencial (series) ou em paralelo (parallel)

const { series, parallel } = require('gulp') //Destructuring modo de execução das tasks


function antes1(callback) {
    console.log('Tarefa Antes 1')
    return callback()
}

function antes2(callback) {
    console.log('Tarefa Antes 2')
    return callback()
}


//src() -> Mapeia os arquivos a serem utilizados, no exemplo abaixo
/*
        pipe() -> 'Transforma' os arquivos e executa uma tarefa executada dentro da função pipe.
             Neste caso, copiar os arquvios da pastaA para a pasta B
            */
const copiar = (callback) => {
    gulp.src(['pastaA/arquivo1.txt', 'pastaA/arquivo2.txt'])
        .pipe(gulp.dest('pastaB'))
    return callback()
}

const copiar2 = (callback) => {
    gulp.src('pastaA/**/*.txt') //Pega qualquer arquivo de extensão txt dentro da pastaA e copia para a pastaC
        .pipe(gulp.dest('pastaC'))
    return callback()
}

const fim = callback => {
    console.log('Tarefa fim')
    return callback()
}


module.exports.default = series(
    parallel(antes1, antes2),
    parallel(copiar, copiar2),
    fim
)
/*
    Executando as funções antes1 e antes2 em paralelo.
    As outras funções serão executadas após o fim da execução de antes1 e antes2
*/
