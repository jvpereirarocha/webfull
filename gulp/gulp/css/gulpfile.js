const gulp = require('gulp')
const series = gulp.series
const parallel = gulp.parallel
const sass = require('gulp-sass')
const uglifycss = require('gulp-uglifycss')
const concat = require('gulp-concat')


const loadSass = () => {
    return gulp.src('src/sass/index.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(uglifycss({
            "uglifyComments": true
        }))
        .pipe(concat('estilo.min.css'))
        .pipe(gulp.dest('build/css'))
}

const copyHtmlFile = () => {
    return gulp.src('src/index.html')
    .pipe(gulp.dest('build'))
}

exports.default = parallel(loadSass, copyHtmlFile)
